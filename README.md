# Download and RUN #
```
mkdir -p /root/tools
cd /root/tools/
wget -O tooldata.sh https://bitbucket.org/soceduteams/mariadb/raw/c11e68fc495a48eb50dd653a5e37c76b1461dcfd/tooldata.sh
chmod +x tooldata.sh
```

### Tao MySQL user/pass va MySQL database va grant privileges database ###

```
/root/tools/tooldata.sh setuserdb
```

### Thay doi mat khau user MySQL da tao ###

```
/root/tools/tooldata.sh setpass
```

### Xoa user MySQL ###

```
/root/tools/tooldata.sh deluser
```

### Show Grand ###

```
/root/tools/tooldata.sh showgrants
```

Xem huong dan tieng viet tai: https://forums.wbuy.pro/t/cach-tao-user-pass-database-nhanh-tren-centminmod-nginx/180
